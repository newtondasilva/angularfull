# Project CI&T Telecom

CI&T challenge from Newton da Silva

Based on a starter kit from [https://github.com/ngx-rocket/starter-kit]


# Getting started

1. Go to project folder and install dependencies:
 ```bash
 npm install
 ```
 
2. Launch development server, and open `localhost:4200` in your browser:
 ```bash
 npm start
 ```
 
# Project structure

```
src/                         project source code
|- app/                      app components
|  |- core/                  core module
|  |- home/                  project page
|    |- api.service.ts       API Service
|    |- home.component.html  Home component HTML
|    |- home.component.scss  SCSS from home component
|    |- home.component.ts    Home component
|  |- result.ts              Result interface used in home
|- assets/                   app assets (images)
|  |- images/                images
|- environments/             values for build environments
|- theme/                    app global scss variables and theme
|  |- theme-variables.scss   Theme variables
|  |- theme.scss             Global theme
|- index.html                html entry point
|- main.scss                 global style entry point
|- main.ts                   app entry point
|- polyfills.ts              polyfills needed by Angular
```