import { Component, OnInit } from '@angular/core';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, ReactiveFormsModule } from '@angular/forms';
import { NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { Result } from '../result';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})


export class HomeComponent implements OnInit {

  ddds: any[];
  pricing: any[];
  plans: any[];
  results: Result[] = new Array;
  
  loginForm: FormGroup;
  modal: NgbModalRef;

  isLoading = false;
  clickedField: number;
  noPlan = 'Sem plano';
  
  constructor(private router: Router,
              private formBuilder: FormBuilder,
              private modalService: NgbModal,
              private http: HttpClient,
              private apiService: ApiService,
            ) {

    this.createForm();
    
  }

  ngOnInit() {
    this.isLoading = true;
    
    this.apiService.getFees()
    .pipe()
    .subscribe(data => {
      this.ddds = data.data;
    });

    this.apiService.getPlans()
    .pipe()
    .subscribe(data => {
      this.plans = data.data;
      this.reset();
    });

    this.apiService.getPrincing()
    .pipe()
    .subscribe(data => {
      this.pricing = data.data;
    });
    
  
    this.isLoading = false;
    
  }

  reset(){
    this.results.length = 0;
    
    this.plans.forEach(item => {
      this.results.push({plan : item.plan, value : 0, time : item.time});    
    })

    this.results.push({plan: this.noPlan, value: 0, time: 0})    
  }

  openModal(content : any, field : any){
    this.clickedField = field;
    this.modal = this.modalService.open(content);
  }

  choose(item : any){
    
    if(this.clickedField == 1) 
      this.loginForm.patchValue({origem : item.ddd});
    else 
      this.loginForm.patchValue({destino : item.ddd});
    
      this.modal.close();
      this.update();
  }

  
  private createForm() {
    this.loginForm = this.formBuilder.group({
      origem: ['', Validators.required],
      destino: ['', Validators.required],
      minutos: ['']
    });
  }

  setMinute(minute : number){
    this.loginForm.patchValue({minutos : minute});
    
    if(minute) this.update();
    else this.reset();
      
  }

  update(){
    
    if(!this.loginForm.valid) return;

    var origem = this.loginForm.get('origem').value;
    var destino = this.loginForm.get('destino').value;
    var minutos = this.loginForm.get('minutos').value;
    
    var minutesLeft = 0;
    var total = 0;

    this.results.length = 0;

    this.pricing.forEach(price => {

      // Origin and destiny match
      if(price.origin == origem && price.destiny == destino){
        
        // Calcs each plan
        this.plans.forEach(plan => {
          minutesLeft = minutos - plan.time

          
        
          // Calcs overpriced in case of any minute left
          if(minutesLeft){
            total = this.calculate(price.price, minutesLeft, true)
          }
          
          this.results.push({
            plan : plan.plan,
            time : plan.time,
            value : total,
          })
        });

        // Calcs with no plan
        total = this.calculate(price.price, minutos, false);
        this.results.push({
          plan  : this.noPlan,
          time : 0,
          value : total,
        });
      }
    });

    if(this.results.length == 0) this.reset();

  }

  calculate(price : number, minutes : number, over : boolean){
    if(minutes < 0) return 0;

    if(over) price *= 1.1;

    return minutes * price;
  }
 
  

}
